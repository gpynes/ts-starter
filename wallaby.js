module.exports = function (wallaby) {

    return {
        files: [
            'src/**/*.ts'
        ],
        tests: [
            'tests/**/*.test.ts'
        ],
        compilers: {
            '**/*.ts?(x)': wallaby.compilers.typeScript({
                useStandardDefaults: true,
                module: 'commonjs',
                jsx: 'react'
            }),
        },
        env: {
            type: 'node',
            runner: 'node'
        },
        debug: true,
        testFramework: 'mocha'
    }
}